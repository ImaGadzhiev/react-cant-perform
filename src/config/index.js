export const HOME_PAGE = '/';
export const ABOUT_PAGE = '/about';


export const KEY_API_V3_AUTH = 'e88e0c0879d78fd4250835c98d191ef1';
export const MOVIE_DB_GET = `https://api.themoviedb.org/3/movie/550?api_key=${KEY_API_V3_AUTH}&language=ru`

export const errorMessage = `
  index.js:1 Warning: Can't perform a React state update on an unmounted component. This is a no-op, but it indicates a memory leak in your application. To fix, cancel all subscriptions and asynchronous tasks in a useEffect cleanup function.
`;
