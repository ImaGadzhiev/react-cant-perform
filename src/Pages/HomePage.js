import React, { useState, useEffect } from 'react';

import { MOVIE_DB_GET } from '../config';

const HomePage = () => {
  // movie - react-состояние;
  // setMovie - функция обновления react-состояния
  const [ movie, setMovie ] = useState(null);


  useEffect(() => {
    let cleanupFunction = false;
    const fetchData = async () => {
      try {
        const response = await fetch(MOVIE_DB_GET);
        const result = await response.json();
        console.log(result, 'result')

        // непосредственное обновление состояния
        if(!cleanupFunction) setMovie(result);
      } catch (e) {
        console.error(e.message)
      }
    };

    fetchData();

    // функция очистки useEffect
    return () => cleanupFunction = true;
  }, []);

  const descriptionMovie = () => {
    const {
      overview,
      budget,
      production_companies,
      production_countries
    } = movie;

    return (
      <React.Fragment>
        <p>Описание: {overview}</p>
        <p>Бюджет: {budget}</p>

        {
          production_companies ? (
            <ul>
              <h3>Продакшн компания: </h3>
              {
                production_companies.map((item, index) => {
                  return (
                    <li key={index}>{item.name}</li>
                  )
                })
              }
            </ul>
          ) : false
        }

        {
          production_countries ? (
            <ul>
              <h3>Продакшн страна: </h3>
              {
                production_countries.map((item, index) => {
                  return (
                    <li key={index}>Продакшн страна: {item.name}</li>
                  )
                })
              }
            </ul>
          ) : false
        }
      </React.Fragment>
    )
  };

  return (
    <main>
      <h2>Главная страница</h2>
      <p>Описание фильма</p>

      {
        movie ? descriptionMovie() : false
      }
    </main>
  )
};

export default HomePage;
