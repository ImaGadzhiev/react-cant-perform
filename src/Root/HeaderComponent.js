import React from 'react';
import {Link} from "react-router-dom";
import {ABOUT_PAGE, HOME_PAGE} from "../config";

const HeaderComponent = () => {
  return (
    <header>
      <nav className="header">
        <ul>
          <li><Link to={HOME_PAGE}>Домой</Link></li>
          <li><Link to={ABOUT_PAGE}>О нас</Link></li>
        </ul>
      </nav>
    </header>
  )
};

export default HeaderComponent
